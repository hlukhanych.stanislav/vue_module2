import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore/lite'

const firebaseConfig = {
    apiKey: "AIzaSyAvzwng_zKDRwP2_AwC9r573uJm48KDTOE",
    authDomain: "module2-a7e40.firebaseapp.com",
    projectId: "module2-a7e40",
    storageBucket: "module2-a7e40.appspot.com",
    messagingSenderId: "706296675220",
    appId: "1:706296675220:web:2e5988ee140e4deba06d81"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app)
export default db