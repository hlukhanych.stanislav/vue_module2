import { createStore } from 'vuex'
import phones from "@/store/modules/phones";

export default createStore({
  namespaced: true,
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    phones,
  }
})
