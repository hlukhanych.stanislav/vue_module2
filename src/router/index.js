import { createRouter, createWebHistory } from 'vue-router'
import CatalogPage from "@/components/CatalogPage.vue";
import HomePage from "@/components/HomePage.vue";
import AboutPage from "@/components/AboutPage.vue";
import ShowPage from "@/components/ShowPage.vue";
import EditPage from "@/components/EditPage.vue";
import AddPage from "@/components/AddPage.vue";

const routes = [
  {
    path: '/catalog',
    name: 'catalog',
    component: CatalogPage
  },
  {
    path: '/',
    name: 'home',
    component: HomePage
  },
  {
    path: '/about',
    name: 'about',
    component: AboutPage
  },
  {
    path: '/:id',
    name: 'show',
    component: ShowPage
  },
  {
    path: '/:id/edit',
    name: 'edit',
    component: EditPage
  },
  {
    path: '/add',
    name: 'add',
    component: AddPage
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
